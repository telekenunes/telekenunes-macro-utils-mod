import { ApiInitializer } from "../commons/ApiInitializer.js";
import { SummonerEidolonSyncValidator } from "./SummonerEidolonSyncValidator.js";
import { ActorUtils } from "../commons/ActorUtils/ActorUtils.js";

class SummonerEidolonSync {

    static async syncEidolon(summonerUuid, eidolonUuid) {
        try {
            SummonerEidolonSyncValidator.validateInputs({
                "summonerUuid": summonerUuid,
                "eidolonUuid": eidolonUuid
            });
            const summonerActor = await fromUuid(summonerUuid);
            const eidolonActor = await fromUuid(eidolonUuid);
            const updateData = SummonerEidolonSync.#prepareUpdateData(summonerActor, eidolonActor);
            SummonerEidolonSync.#updateEidolon({
                eidolonActor: eidolonActor,
                updateData: updateData
            });
        } catch(e) {
            console.error(e+"\n\nAborting SummonerEidolonSync.");
        }
        
    }

    static #prepareUpdateData(summonerActor, eidolonActor) {
        const updateData = {
            "_id": eidolonActor["_id"],
            "system": {
                "custom": {
                    "eidolon": {
                        "sync": {
                            "flags": SummonerEidolonSync.#prepareFlagsUpdateData(summonerActor),
                            "system": {
                                "attributes": SummonerEidolonSync.#prepareAttributesUpdateData(summonerActor),
                                "details": SummonerEidolonSync.#prepareDetailsUpdateData(summonerActor),
                                "martial": SummonerEidolonSync.#prepareMartialUpdateData(summonerActor),
                                "saves": SummonerEidolonSync.#prepareSavesUpdateData(summonerActor),
                                "skills": SummonerEidolonSync.#prepareSkillsUpdateData(summonerActor)
                            }
                        }
                    }
                }
            }
        };

        return updateData;
    };

    static #prepareAttributesUpdateData(summonerActor) {
        const summonerAttributesData = ActorUtils.getAttributes(summonerActor);
        const attributesUpdateData = {
            "hp" : {
                "value" : summonerAttributesData.hp.value,
                "max" : summonerAttributesData.hp.max
            },
            "perception": {
                "rank": summonerAttributesData.perception.rank
            }
        };

        return attributesUpdateData;
    }

    static #prepareDetailsUpdateData(summonerActor) {
        const summonerDetailsData = ActorUtils.getDetails(summonerActor);
        const detailsUpdateData = {
            "level" : {
                "value" : summonerDetailsData.level.value
            }
        };

        return detailsUpdateData;
    }

    static #prepareFlagsUpdateData(summonerActor) {
        const weaponSpecializationFlag = "feature:weapon-specialization";
        const psychicWeaponSpecializationFlag = "feature:psychic-weapon-specialization";
        const flagsUpdateData = {
            "weaponSpecialization": ActorUtils.hasFlag({actor: summonerActor, flag: weaponSpecializationFlag}) ? ActorUtils.getFlagValue({actor: summonerActor, flag: weaponSpecializationFlag}) : false,
            "psychicWeaponSpecialization": ActorUtils.hasFlag({actor: summonerActor, flag: psychicWeaponSpecializationFlag}) ? ActorUtils.getFlagValue({actor: summonerActor, flag: psychicWeaponSpecializationFlag}) : false
        }

        return flagsUpdateData;
    }

    static #prepareMartialUpdateData(summonerActor) {
        const summonerMartialData = ActorUtils.getMartial(summonerActor);
        const martialUpdateData = {
            "advanced":{
                "rank": summonerMartialData.advanced.rank
            },
            "heavy":{
                "rank": summonerMartialData.heavy.rank
            },
            "light":{
                "rank": summonerMartialData.light.rank
            },
            "martial":{
                "rank": summonerMartialData.martial.rank
            },
            "medium":{
                "rank": summonerMartialData.medium.rank
            },
            "simple":{
                "rank": summonerMartialData.simple.rank
            },
            "unarmed":{
                "rank": summonerMartialData.unarmed.rank
            },
            "unarmored":{
                "rank": summonerMartialData.unarmored.rank
            }
        };

        return martialUpdateData;
    }

    static #prepareSavesUpdateData(summonerActor) {
        const summonerSavesData = ActorUtils.getSaves(summonerActor);
        const savesUpdateData = {
            "fortitude" : {
                "rank" : summonerSavesData.fortitude.rank
            },
            "reflex" : {
                "rank" : summonerSavesData.reflex.rank
            },
            "will" : {
                "rank" : summonerSavesData.will.rank
            }
        };

        return savesUpdateData;
    }

    static #prepareSkillsUpdateData(summonerActor) {
        const summonerSkillsData = ActorUtils.getSkills(summonerActor);
        const skillsUpdateData = {
            "acr" : {
                "rank" : summonerSkillsData.acr.rank
            },
            "arc" : {
                "rank" : summonerSkillsData.arc.rank
            },
            "ath" : {
                "rank" : summonerSkillsData.ath.rank
            },
            "cra" : {
                "rank" : summonerSkillsData.cra.rank
            },
            "dec" : {
                "rank" : summonerSkillsData.dec.rank
            },
            "dip" : {
                "rank" : summonerSkillsData.dip.rank
            },
            "itm" : {
                "rank" : summonerSkillsData.itm.rank
            },
            "med" : {
                "rank" : summonerSkillsData.med.rank
            },
            "nat" : {
                "rank" : summonerSkillsData.nat.rank
            },
            "occ" : {
                "rank" : summonerSkillsData.occ.rank
            },
            "prf" : {
                "rank" : summonerSkillsData.prf.rank
            },
            "rel" : {
                "rank" : summonerSkillsData.rel.rank
            },
            "soc" : {
                "rank" : summonerSkillsData.soc.rank
            },
            "ste" : {
                "rank" : summonerSkillsData.ste.rank
            },
            "sur" : {
                "rank" : summonerSkillsData.sur.rank
            },
            "thi" : {
                "rank" : summonerSkillsData.thi.rank
            }
        };

        return skillsUpdateData;
    }

    static #updateEidolon({eidolonActor, updateData}) {
        const updateOptions = {diff:true, render:true};
        
        eidolonActor.update(updateData, updateOptions);
    }
}

Hooks.once("ready", () => {
    try {
        ApiInitializer.initializeApi();
        game.modules.get("telekenunes-macro-utils-mod").api.SummonerEidolonSync = SummonerEidolonSync;
    } catch(e) {
        console.error("SummonerEidolonSync: Could not create API.");
    }
});