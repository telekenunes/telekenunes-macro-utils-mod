import { TypeChecker } from "../commons/TypeChecker.js";

export class SummonerEidolonSyncValidator {

    static #errorTypeMismatch = "SummonerEidolonSyncValidator: Type Mismatch Error: ";

    static validateInputs({summonerUuid, eidolonUuid}) {

        SummonerEidolonSyncValidator.#validateSummonerUuid(summonerUuid);
        SummonerEidolonSyncValidator.#validateEidolonUuid(eidolonUuid);
    }
    
    static #validateSummonerUuid(characterUuid) {

        if(!TypeChecker.isString(characterUuid)) {
            throw SummonerEidolonSyncValidator.#errorTypeMismatch + "validateSummonerUuid.summonerUuid must be a string. Was " + JSON.stringify(characterUuid);
        }
    };

    static #validateEidolonUuid(characterUuid) {

        if(!TypeChecker.isString(characterUuid)) {
            throw SummonerEidolonSyncValidator.#errorTypeMismatch + "validateEidolonUuid.eidolonUuid must be a string. Was " + JSON.stringify(characterUuid);
        }
    };
}