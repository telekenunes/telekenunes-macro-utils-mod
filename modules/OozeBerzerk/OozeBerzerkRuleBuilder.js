import { AbilityScoreUtils } from "../commons/AbilityScoreUtils.js";
import { ActorUtils } from "../commons/ActorUtils/ActorUtils.js"
import { AbilityScores } from "../commons/enums/AbilityScores.js";

export class OozeBerzerkRuleBuilder {

    static buildAbilityScoreRules(actor) {
        const actorAbilityScores = ActorUtils.getAbilityScores(actor);
        const actorLevel = ActorUtils.getLevel(actor);

        let newPhysicalScores = {};
        newPhysicalScores[AbilityScores.STR] = AbilityScoreUtils.applyAbilityBoosts(
            actorAbilityScores[AbilityScores.STR].value, 
            AbilityScoreUtils.calculateAbilityBoosts(actorAbilityScores[AbilityScores.INT].value)
        );

        newPhysicalScores[AbilityScores.DEX] = AbilityScoreUtils.applyAbilityBoosts(
            actorAbilityScores[AbilityScores.DEX].value, 
            AbilityScoreUtils.calculateAbilityBoosts(actorAbilityScores[AbilityScores.WIS].value)
        );

        newPhysicalScores[AbilityScores.CON] = AbilityScoreUtils.applyAbilityBoosts(
            actorAbilityScores[AbilityScores.CON].value, 
            AbilityScoreUtils.calculateAbilityBoosts(actorAbilityScores[AbilityScores.CHA].value)
        );

        const pathPrefix = "data.abilities.";
        const pathSuffix = ".value";
        const basePhysicalAbilityRule = {
            key:"ActiveEffectLike",
            mode:"upgrade",
            path:"",
            value:0
        };
        const baseMentalAbilityRule = {
            key:"ActiveEffectLike",
            mode:"downgrade",
            path:"",
            value:0
        };
        
        let rules = [];

        for(const [key, value] of Object.entries(newPhysicalScores)) {
            let rule = JSON.parse( JSON.stringify(basePhysicalAbilityRule) );

            rule.path = pathPrefix + key + pathSuffix;
            rule.value = value;

            rules.push(rule);
        }

        for(const key of AbilityScores.mental) {
            let rule = JSON.parse( JSON.stringify(baseMentalAbilityRule) );

            rule.path = pathPrefix + key + pathSuffix;

            rules.push(rule);
        }

        const hpDiff = OozeBerzerkRuleBuilder.#calculateHpDifference(actorAbilityScores[AbilityScores.CON].value, newPhysicalScores[AbilityScores.CON], actorLevel);

        if(hpDiff !== 0) {
            const tempHpRule = {
                key:"TempHP",
                value:hpDiff
            };

            const flatHpRule = {
                key:"FlatModifier",
                selector:"hp",
                value: -hpDiff
            };

            rules.push(tempHpRule);
            rules.push(flatHpRule);
        }

        return rules;
    }

    static #calculateHpDifference(oldCon, newCon, actorLevel) {
        const oldMod = AbilityScoreUtils.calculateModFromValue(oldCon);
        const newMod = AbilityScoreUtils.calculateModFromValue(newCon);

        return (newMod - oldMod) * actorLevel;
    }
}