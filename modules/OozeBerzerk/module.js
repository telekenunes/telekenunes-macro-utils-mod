import { ActorUtils } from "../commons/ActorUtils/ActorUtils.js";
import { ApiInitializer } from "../commons/ApiInitializer.js";
import { OozeBerzerkRuleBuilder } from "./OozeBerzerkRuleBuilder.js";
import { OozeBerzerkValidator } from "./OozeBerzerkValidator.js";

class OozeBerzerk {

    static #UUID_BERZERK_STATUS = "Compendium.telekenunes-macro-utils-mod.telekenunes-macro-items.E1mv6uiOWCCNzFuW";

    static async goBerzerk(actorUuid) {
        try {
            OozeBerzerkValidator.validateInputs({
                "actorUuid": actorUuid,
            });

            const targetActor = await fromUuid(actorUuid);
            const actorIsBerzerk = ActorUtils.hasCompendiumItem(targetActor, OozeBerzerk.#UUID_BERZERK_STATUS);

            if(actorIsBerzerk) {
                return;
            }

            const berzerkEffect = JSON.parse(JSON.stringify(await fromUuid(OozeBerzerk.#UUID_BERZERK_STATUS)));
            const existingRules = berzerkEffect.data.rules || [];
            const abilityRules = OozeBerzerkRuleBuilder.buildAbilityScoreRules(targetActor);

            berzerkEffect.data.rules = existingRules.concat(abilityRules);
            targetActor.createEmbeddedDocuments("Item", [ berzerkEffect ]);

        } catch(e) {
            console.error(e+"\n\nAborting OozeBerzerk.");
        }
        
    }

    
}


Hooks.once("ready", () => {
    try {
        ApiInitializer.initializeApi();
        game.modules.get("telekenunes-macro-utils-mod").api.OozeBerzerk = OozeBerzerk;
    } catch(e) {
        console.error("OozeBerzerk: Could not create API.");
    }
});