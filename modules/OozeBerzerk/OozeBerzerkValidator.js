import { TypeChecker } from "../commons/TypeChecker.js";

export class OozeBerzerkValidator {

    static #errorTypeMismatch = "OozeBerzerkValidator: Type Mismatch Error: ";

    static validateInputs({actorUuid}) {
        if(!TypeChecker.isString(actorUuid)) {
            throw OozeBerzerkValidator.#errorTypeMismatch + "validateInputs.actorUuid must be a string. Was " + JSON.stringify(actorUuid);
        }
    }
}