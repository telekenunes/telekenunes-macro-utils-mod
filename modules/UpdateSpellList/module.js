import { ApiInitializer } from "../commons/ApiInitializer.js";
import { UpdateSpellListValidator } from "./UpdateSpellListValidator.js";
import { ActorUtils } from "../commons/ActorUtils/ActorUtils.js";

class UpdateSpellList {

    static updateSpellList(characterName, spellList, spellListParams) {
        try {
            UpdateSpellListValidator.validateInputs({
                "characterName": characterName,
                "spellList": spellList,
                "spellListParams": spellListParams
            });

            const targetActor = ActorUtils.findActor(characterName);
            const spellcastingEntry = UpdateSpellList.#findSpellcastingEntry(targetActor, spellListParams);
            const updateData = UpdateSpellList.#prepareUpdateData(spellcastingEntry, spellList);
            UpdateSpellList.#updatePreparedSpells(spellcastingEntry, updateData);
        } catch(e) {
            console.error(e+"\n\nAborting SpellListUpdate.");
        }
        
    }

    static #findSpellcastingEntry(actor, spellListParams) {
        const actorItems = actor.data.items;
        for(const [key, item] of actorItems.entries()) {
            if(UpdateSpellList.#isCorrectSpellcastingEntry(item, spellListParams)) {
                return item;
            }
        };
        
        throw fatalErrorStr + "Could not find an appropriate spellcasting entry for actor " + actor.name;
    };

    static #isCorrectSpellcastingEntry(item, spellListParams) {
        return item.type.toLowerCase() === "spellcastingentry"
            && item.tradition.toLowerCase() === spellListParams.tradition.toLowerCase()
            && item.ability.toLowerCase() === spellListParams.ability.toLowerCase()
            && item.isPrepared === true
            && item.isFlexible === false;
    };

    static #prepareUpdateData(spellcastingEntry, spellList) {
        const knownSpells = spellcastingEntry.spells;
        
        const updateData = {
            data: {
                "_id": spellcastingEntry.data["_id"],
                "slots": {}
            }
        };
    
        spellList.forEach(entry => {
            if(entry.spellLevel <= spellcastingEntry.highestLevel) {
                let slotIndex = "slot"+entry.spellLevel;
                updateData.data.slots[slotIndex] = UpdateSpellList.#createSpellSlotsFromEntry(entry, knownSpells);
                let i = entry.spells.length;
                let currentPreparedSpells = spellcastingEntry.data.data.slots[slotIndex].prepared;
                while(currentPreparedSpells[i]) {
                    updateData.data.slots[slotIndex].prepared[i] = {
                        "id": null,
                        "name": "Empty Slot (drag spell here)",
                        "prepared": false
                    };
                    i++;
                }
            }
        });
    
        return updateData;
    };

    static #createSpellSlotsFromEntry(entry, knownSpells) {
        const defaultSpellSlot = {
            "id": null,
            "name": "Empty Slot (drag spell here)",
            "prepared": false
        };
    
        const spellSlots = {};
        spellSlots.max = entry.slotsPerDay;
        spellSlots.prepared = {};
        let spellCount = 0;
        entry.spells.forEach(spellName => {
            try {
                const spellKey = UpdateSpellList.#findKeyFromSpellName(spellName, knownSpells);
                if(knownSpells.get(spellKey).data.data.level > entry.spellLevel) {
                    throw "Warning: Spell with name " + spellName + "was prepared at lower than base level.";
                }
                spellSlots.prepared[spellCount] = {"id": spellKey}; 
            } catch(e) {
                console.warn(e + "\nUsing emptry Spell Entry instead.");
                spellSlots.prepared[spellCount] = {...defaultSpellSlot};
            }
            spellCount++;
        });
    
        return spellSlots;
    };
    
    static #findKeyFromSpellName(spellName, spellsMap) {
        for(const[key, spell] of spellsMap.entries()) {
            if(spellName === spell.data.name) {
                return key;
            }
        }
    
        throw "Warning: Could not find spell key for spell with name: " + spellName;
    }
    
    static #updatePreparedSpells(spellcastingEntry, updateData) {
        const updateOptions = {diff:true, render:true};
        
        spellcastingEntry.update(updateData, updateOptions);
    };
}


Hooks.once("ready", () => {
    try {
        ApiInitializer.initializeApi();
        game.modules.get("telekenunes-macro-utils-mod").api.UpdateSpellList = UpdateSpellList;
    } catch(e) {
        console.error("UpdateSpellList: Could not create API.");
    }
});