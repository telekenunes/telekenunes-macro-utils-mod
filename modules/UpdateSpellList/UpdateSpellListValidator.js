import { TypeChecker } from "../commons/TypeChecker.js";

export class UpdateSpellListValidator {

    static #errorTypeMismatch = "UpdateSpellListValidator: Type Mismatch Error: ";
    static #invalidSpellList = "UpdateSpellListValidator: invalid spell list error: ";
    static #errorSpellListParams = "UpdateSpellListValidator: invalid spell list params: "

    static #spellLevel = "spellLevel";
    static #slotsPerDay = "slotsPerDay";
    static #spells = "spells";
    static #tradition = "tradition";
    static #ability = "ability";
    
    static #validateCharacterName(characterName) {

        if(!TypeChecker.isString(characterName)) {
            throw UpdateSpellListValidator.#errorTypeMismatch + "validatecharacterName.characterName must be a string. Was " + JSON.stringify(characterName);
        }
    };

    static #validateSpell(spell) {
        if(!TypeChecker.isString(spell)) {
            throw UpdateSpellListValidator.#invalidSpellList + "validateSpell.spell must be a string. Was " + JSON.stringify(spell);
        }
    }

    static #validateSpellListEntry(spellListEntry) {
        if(!TypeChecker.isObject(spellListEntry)) {
            throw UpdateSpellListValidator.#invalidSpellList + "validateSpellListEntry.spellListEntry must be an Object. Was " + JSON.stringify(spellListEntry);
        }

        if(!TypeChecker.isNonNegativeInteger(spellListEntry[UpdateSpellListValidator.#spellLevel])) {
            throw UpdateSpellListValidator.#invalidSpellList + "validateSpellListEntry.spellListEntry." + UpdateSpellListValidator.#spellLevel + " must be a non negative integer. Was " + JSON.stringify(spellListEntry[UpdateSpellListValidator.#spellLevel]);
        }

        if(!TypeChecker.isNonNegativeInteger(spellListEntry[UpdateSpellListValidator.#slotsPerDay])) {
            throw UpdateSpellListValidator.#invalidSpellList + "validateSpellListEntry.spellListEntry." + UpdateSpellListValidator.#slotsPerDay + " must be a non negative integer. Was " + JSON.stringify(spellListEntry[UpdateSpellListValidator.#slotsPerDay]);
        }

        if(!TypeChecker.isArray(spellListEntry[UpdateSpellListValidator.#spells])) {
            throw UpdateSpellListValidator.#invalidSpellList + "validateSpellListEntry.spellListEntry." + UpdateSpellListValidator.#spells + " must be an Array. Was " + JSON.stringify(spellListEntry[UpdateSpellListValidator.#spells]);
        }

        const spells = spellListEntry[UpdateSpellListValidator.#spells];

        if(spells.length > spellListEntry[UpdateSpellListValidator.#slotsPerDay]) {
            throw UpdateSpellListValidator.#invalidSpellList + "validateInputsEntry.spellListEntry has more spells than allowed per day. Was " + JSON.stringify(spellListEntry);
        }

        for(const spell of spells) {
            UpdateSpellListValidator.#validateSpell(spell);
        }
    }

    static #validateSpellList(spellList) {
        if(!TypeChecker.isArray(spellList)) {
            throw UpdateSpellListValidator.#errorTypeMismatch + "validateInputs.spellList must be an Array.";
        }

        if(spellList.length === 0) {
            throw UpdateSpellListValidator.#invalidSpellList + "validateInputs.spellList cannot be empty.";
        }

        for(const spellListEntry of spellList) {
            UpdateSpellListValidator.#validateSpellListEntry(spellListEntry);
        }
    }

    static #validateSpellListParams(spellListParams) {
        if(!TypeChecker.isObject(spellListParams)) {
            throw UpdateSpellListValidator.#errorSpellListParams + "validateSpellListParams.spellListParams must be an object. Was " + JSON.stringify(spellListParams);
        }

        if(!TypeChecker.isString(spellListParams[UpdateSpellListValidator.#tradition])) {
            throw UpdateSpellListValidator.#errorSpellListParams + "validateSpellListParams.spellListParams." + UpdateSpellListValidator.#tradition + " must be a string.";
        }

        if(!TypeChecker.isString(spellListParams[UpdateSpellListValidator.#ability])) {
            throw UpdateSpellListValidator.#errorSpellListParams + "validateSpellListParams.spellListParams." + UpdateSpellListValidator.#ability + " must be a string.";
        }
    }

    static validateInputs({characterName, spellList, spellListParams}) {

        UpdateSpellListValidator.#validateCharacterName(characterName);
        UpdateSpellListValidator.#validateSpellList(spellList);
        UpdateSpellListValidator.#validateSpellListParams(spellListParams);
    }
}