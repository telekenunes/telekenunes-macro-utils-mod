import { TypeChecker } from "./TypeChecker.js";

export class AbilityScoreUtils {
    
    static calculateAbilityBoosts(abilityValue) {
        
        if(!TypeChecker.isNonNegativeInteger(abilityValue)) {
            throw "Could not calculate Ability Boosts. Ability value must be non-negative integer, but was: " + abilityValue;
        }

        if(abilityValue < 10 && abilityValue%2 === 0) {
            return -abilityValue / 2;
        } else if(abilityValue <= 18 && abilityValue%2 === 0) {
            return (abilityValue - 10)/2;
        } else if (abilityValue > 18) {
            return abilityValue - 18 + 4;
        }

        throw "Could not calculate Ability Boosts. Ability value was invalid: " + abilityValue;
    }

    static applyAbilityBoosts(abilityValue, numberOfBoosts) {

        if(!TypeChecker.isNonNegativeInteger(abilityValue)) {
            throw "Could not apply Ability Boosts. Ability value must be non-negative integer, but was: " + abilityValue;
        }

        if(!TypeChecker.isInteger(numberOfBoosts)) {
            throw "Could not apply Ability Boosts. number of boosts must be an integer, but was: " + numberOfBoosts;
        }

        if(numberOfBoosts >= 0) {
            return AbilityScoreUtils.#applyBoosts(abilityValue, numberOfBoosts);
        } else {
            return AbilityScoreUtils.#applyPenalties(abilityValue, -numberOfBoosts);
        }
    }

    static #applyBoosts(abilityValue, numberOfBoosts) {
        for(let i = 0; i < numberOfBoosts; i++) {
            if(abilityValue < 18) {
                abilityValue += 2;
            } else {
                abilityValue += 1;
            }
        }

        return abilityValue;
    }

    static #applyPenalties(abilityValue, numberOfPenalties) {
        return abilityValue - 2 * numberOfPenalties;
    }

    static calculateModFromValue(abilityValue) {
        return Math.floor( (abilityValue - 10) / 2 )
    }
}