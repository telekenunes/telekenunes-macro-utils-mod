import { TypeChecker } from "./TypeChecker.js";

export class ApiInitializer {
    static initializeApi() {

        const module = game.modules.get("telekenunes-macro-utils-mod");
        
        if(!TypeChecker.isObject(module)) {
            throw "initializeApi: Could not find module while initializing API";
        }
        
        if(!TypeChecker.isObject(module.api)) {
            game.modules.get("telekenunes-macro-utils-mod").api = {};
        }
    };
}