import { TypeChecker } from "../TypeChecker.js";
import { PCActorUtils } from "./PCActorUtils.js";

export class ActorUtils {
    
    static #PC_ACTOR_TYPE = "character";
    static #NPC_ACTOR_TYPE = "npc";

    static findActor(actorName) {
        const actorList = game.actors;
        for(const actor of actorList) {
            if(actor.name === actorName) {
                return actor;
            }
        };

        throw fatalErrorStr + "Actor with name " + actorName + " could not be found.";
    }

    static #getActorType(actor) {
        return actor?.type;
    }

    static getAbilityScores(actor) {
        
        if(this.isPCActor(actor)) {
            return PCActorUtils.getAbilityScores(actor);
        };

        throw "Could not get actor's Ability Scores. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getAttributes(actor) {
        
        if(this.isPCActor(actor)) {
            return PCActorUtils.getAttributes(actor);
        };

        throw "Could not get actor's attributes. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getDetails(actor) {
    
        if(this.isPCActor(actor)) {
            return PCActorUtils.getDetails(actor);
        };

        throw "Could not get actor's details. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getDomainFlagValue({actor, flag, domain}) {
        
        if(!TypeChecker.isString(flag)) {
            throw "Could not get actor's flag value. flag must be a string. Was: " + flag;
        }

        if(!TypeChecker.isString(domain)) {
            throw "Could not get actor's flag value. domain must be a string. Was: " + domain;
        }

        if(!TypeChecker.isObject(actor)) {
            throw "Could not get actor's flag value. Actor must be an object. Was: " + actor;
        }

        if(this.isPCActor(actor)) {
            return PCActorUtils.getDomainFlagValue({pcActor: actor, flag: flag, domain: domain});
        }

        throw "Could not get actor's flag value. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getFlags(actor) {
        
        if(this.isPCActor(actor)) {
            return PCActorUtils.getFlags(actor);
        };

        throw "Could not get actor's flags. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getFlagValue({actor, flag}) {
        
        return this.getDomainFlagValue({actor: actor, flag: flag, domain: "all"});
    }

    static getFocusPool(actor) {
        
        if(this.isPCActor(actor)) {
            return PCActorUtils.getFocusPool(actor);
        };

        throw "Could not get actor's Focus Pool. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getItems(actor) {
        
        if(this.isPCActor(actor)) {
            return PCActorUtils.getItems(actor);
        };

        throw "Could not get actor's Items. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getLevel(actor) {
        if(this.isPCActor(actor)) {
            return PCActorUtils.getLevel(actor);
        };

        throw "Could not get actor's level. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getMartial(actor) {
        if(this.isPCActor(actor)) {
            return PCActorUtils.getMartial(actor);
        };

        throw "Could not get actor's martial proficiencies. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getSaves(actor) {
        
        if(this.isPCActor(actor)) {
            return PCActorUtils.getSaves(actor);
        };

        throw "Could not get actor's saves. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getSkills(actor) {
        
        if(this.isPCActor(actor)) {
            return PCActorUtils.getSkills(actor);
        };

        throw "Could not get actor's skills. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static getSpellcastingProficiencies(actor) {
        
        if(this.isPCActor(actor)) {
            return PCActorUtils.getSpellcastingProficiencies(actor);
        };

        throw "Could not get actor's spellcasting proficiencies. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static hasCompendiumItem(actor, itemUuid) {

        if(!TypeChecker.isString(itemUuid)) {
            throw "Cannot determine if actor has compendium item. Item uuid must be a string. Was: " + itemUuid;
        }

        if(!TypeChecker.isObject(actor)) {
            throw "Cannot determine if actor has compendium item. Actor must be an object. Was: " + actor;
        }

        const actorItems = this.getItems(actor);

        for(const [key, item] of actorItems.entries()) {
            if(itemUuid === item?.flags?.core?.sourceId) {
                return true;
            }
        };

        return false;
    }

    static hasFlag({actor, flag}) {
        
        return this.hasDomainFlag({actor: actor, flag: flag, domain: "all"});
    }

    static hasDomainFlag({actor, flag, domain}) {
        
        if(!TypeChecker.isString(flag)) {
            throw "Cannot determine if actor has flag. flag must be a string. Was: " + flag;
        }

        if(!TypeChecker.isString(domain)) {
            throw "Cannot determine if actor has flag. domain must be a string. Was: " + domain;
        }

        if(!TypeChecker.isObject(actor)) {
            throw "Cannot determine if actor has flag. Actor must be an object. Was: " + actor;
        }

        if(this.isPCActor(actor)) {
            return PCActorUtils.hasDomainFlag({pcActor: actor, flag: flag, domain: domain});
        }

        throw "Cannot determine if actor has flag. Uknown Actor type: " + ActorUtils.#getActorType(actor);
    }

    static isPCActor(actor) {
        return ActorUtils.#PC_ACTOR_TYPE === ActorUtils.#getActorType(actor);
    }

    static getCompendiumItem(actor, itemUuid) {

        if(!TypeChecker.isString(itemUuid)) {
            throw "Cannot get actor compendium item. Item uuid must be a string. Was: " + itemUuid;
        }

        if(!TypeChecker.isObject(actor)) {
            throw "Cannot get actor compendium item. Actor must be an object. Was: " + actor;
        }

        const actorItems = this.getItems(actor);

        for(const [key, item] of actorItems.entries()) {
            if(itemUuid === item?.flags?.core?.sourceId) {
                return item;
            }
        };

        throw "Cannot get actor compendium item. Actor does not have item with sourceId: " + itemUuid;
    }
}