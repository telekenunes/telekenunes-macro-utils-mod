export class PCActorUtils {

    static getAbilityScores(pcActor) {
        return pcActor?.system?.abilities;
    }

    static getAttributes(pcActor) {
        return pcActor?.system?.attributes;
    }

    static getDetails(pcActor) {
        return pcActor?.system?.details;
    }
    
    static getDomainFlagValue({pcActor, flag, domain}) {
        return pcActor.flags.pf2e.rollOptions[domain][flag];
    }

    static getFlags(pcActor) {
        return pcActor?.flags;
    }

    static getFocusPool(pcActor) {
        return pcActor?.system?.resources?.focus;
    }

    static getItems(pcActor) {
        return pcActor?.items;
    }

    static getLevel(pcActor) {
        return pcActor?.system?.details?.level?.value;
    }

    static getMartial(pcActor) {
        return pcActor?.system?.martial;
    }

    static getSaves(pcActor) {
        return pcActor?.system?.saves;
    }

    static getSkills(pcActor) {
        return pcActor?.system?.skills;
    }

    static getSpellcastingProficiencies(pcActor) {
        return pcActor?.system?.proficiencies?.traditions;
    }

    static hasDomainFlag({pcActor, flag, domain}) {
        return Object.keys(pcActor.flags.pf2e.rollOptions[domain]).includes(flag);
    }
}