export class TypeChecker {
    
    static isString(str) {
        return str !== null && str !== undefined && typeof str === "string";
    }

    static isArray(arr) {
        return arr !== null && arr !== undefined && typeof arr === "object" && Array.isArray(arr) === true;
    }

    static isObject(obj) {
        return obj !== null && obj !== undefined && typeof obj === "object" && Array.isArray(obj) === false && !(obj instanceof Map);
    }

    static isMap(map) {
        return map !== null && map !== undefined && typeof map === "object"  && map instanceof Map;
    }

    static isInteger(nbr) {
        return nbr !== null && nbr !== undefined && typeof nbr === "number" && Number.isInteger(nbr);
    }

    static isNonNegativeInteger(nbr) {
        return this.isInteger(nbr) && nbr >= 0;
    }
}