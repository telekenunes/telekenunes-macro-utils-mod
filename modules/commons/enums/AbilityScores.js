export class AbilityScores {
    
    static STR = "str";
    static DEX = "dex";
    static CON = "con";
    static INT = "int";
    static WIS = "wis";
    static CHA = "cha";

    static mental = [this.INT, this.WIS, this.CHA];
    static physical = [this.STR, this.DEX, this.CON];
    
}